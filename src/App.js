import React from 'react';
import Landing from './pages/Landing/Landing';
import { BrowserRouter as Router, Route, } from "react-router-dom";
import './App.css';
import PictureRound from './pages/PictureRound/PictureRound';
import RoundOne from './pages/RoundOne/RoundOne';
import RoundTwo from './pages/RoundTwo/RoundTwo';
import MusicRound from './pages/MusicRound/MusicRound';
import FinalScreen from './pages/FinalScreen/FinalScreen'

function App() {
  return (
    <div className="App">
          <Router>
            <Route exact path='/' component={Landing}/>
            <Route exact path='/4QXv@oK4DKzq' component={PictureRound}/>
            <Route exact path='/BMj4f@3aFd' component={RoundOne}/>
            <Route exact path='/veF09b1tjh' component={MusicRound}/>
            <Route exact path='/W8$nIjVoC49' component={RoundTwo}/>
            <Route exact path='/4QfGX!0M@rrp' component={FinalScreen}/>
          </Router>
    </div>
  );
}

export default App;
