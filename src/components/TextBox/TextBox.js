import React, { Component } from 'react'
import './TextBox.css'

export default class TextBox extends Component {
    render() {
        return (
            <form>
                <input className="text-box" type="text" id={this.props.id} name={this.props.name}
                placeholder={this.props.placeholder}></input>
            </form>
        )
    }
}
