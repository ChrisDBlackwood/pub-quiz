import React, { Component } from 'react'
import Button from '../../components/Button/Button'
import TextBox from '../../components/TextBox/TextBox'
import "./Landing.css"
import { Link } from "react-router-dom";

export default class Landing extends Component {
    render() {
        return (
          <div className='landing'>
              <div className='button-container'>
                  <TextBox id="team-name" name="team-name"/>
                  <Link to="/4QXv@oK4DKzq">
                  <Button text="SELECT"/>
                  </Link>
                  </div>
              </div>
        );
    }
}
