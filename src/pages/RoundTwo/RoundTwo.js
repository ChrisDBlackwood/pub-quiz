import React, { Component } from 'react'
import Button from '../../components/Button/Button'
import TextBox from '../../components/TextBox/TextBox'
import { Link } from "react-router-dom";
import "./RoundTwo.css"


export default class PictureRound extends Component {
    render() {
        const amountOfPictureBoxes = 9;
        return (
            <div className='PictureRound'>
                <div className='triangle-container'>
                    <div className='triangle-down'/>
                    <div className='triangle-down'/>
                    <div className='triangle-down'/>
                </div>
                <div className='h1-container'>
                <h1>ROUND TWO</h1>
                </div>
                <div className="picture-round-container">
                    {[...Array(amountOfPictureBoxes)].map((e, i) => 
                        <div className="picture-and-text-container">
                            <h2>QUESTION {i + 1}</h2>
                            <div className="text-box-container">
                                <TextBox id='picture-{i}' name="picture-1" placeholder={i + 1}/>
                            </div>
                    </div>
                    )}
                </div>
                <div class="picture-button-container">
                    <Link to='/4QfGX!0M@rrp'>
                        <Button text="FINISH" />
                    </Link>
                </div>
            </div>
        )
    }
}
