import React, { Component } from 'react'
import Button from '../../components/Button/Button'
import TextBox from '../../components/TextBox/TextBox'
import { Link } from "react-router-dom";
import "./FinalScreen.css"


export default class PictureRound extends Component {
    render() {
        const amountOfPictureBoxes = 9;
        return (
            <div className='PictureRound'>
                <div className='final-screen-container'>
                    <h2>Thank you for playing, your score is being tallied</h2>
                </div>
            </div>
        )
    }
}
