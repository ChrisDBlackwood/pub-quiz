import React, { Component } from 'react'
import Button from '../../components/Button/Button'
import TextBox from '../../components/TextBox/TextBox'
import { Link } from "react-router-dom";
import "./PictureRound.css"

export default class PictureRound extends Component {
    render() {
        const amountOfPictureBoxes = 9;
        return (
            <div className='PictureRound'>
                <div className='triangle-container'>
                    <div className='triangle-down'/>
                    <div className='triangle-down'/>
                    <div className='triangle-down'/>
                </div>
                <div className='h1-container'>
                <h1>PICTURE ROUND</h1>
                </div>
                <div className="picture-round-container">
                    {[...Array(amountOfPictureBoxes)].map((e, i) => 
                        <div className="picture-and-text-container">
                        <div className="picture"/>
                            <div className="text-box-container">
                                <TextBox id='picture-{i}' name="picture-1" placeholder={i + 1}/>
                            </div>
                    </div>
                    )}
                </div>
                <div class="picture-button-container">
                        <Link to='/BMj4f@3aFd'>
                        <Button text="NEXT" />
                    </Link>
                </div>
            </div>
        )
    }
}
